package kz.attractor.microgram.Controller;


import kz.attractor.microgram.DTO.PublicationDTO;
import kz.attractor.microgram.service.PublicationService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/publications")
public class PublicationController {
    private final PublicationService publicationService;

    public PublicationController(PublicationService publicationService) {
        this.publicationService = publicationService;
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public PublicationDTO addPublication(@RequestBody PublicationDTO publicationDTO /*, From Spring Security! User user, */) {
        return publicationService.addPublication( /*user, */ publicationDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePublication(@PathVariable String id) {
        if (publicationService.deleteReview(id))
            return ResponseEntity.noContent().build();

        return ResponseEntity.notFound().build();
    }
}
