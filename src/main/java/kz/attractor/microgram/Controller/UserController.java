package kz.attractor.microgram.Controller;

import io.swagger.annotations.ApiOperation;
import kz.attractor.microgram.DTO.UserDTO;
import kz.attractor.microgram.Model.User;
import kz.attractor.microgram.annotations.ApiPageable;
import kz.attractor.microgram.repository.UserRepository;
import kz.attractor.microgram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Sort;
import org.w3c.dom.events.Event;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private  UserService userServise;
    @Autowired
    private UserRepository ur;


    @ApiPageable
    @GetMapping("/all")
   public Slice<UserDTO> getAllUsers(@ApiIgnore Pageable pageable){return userServise.findUsers(pageable); }

//   @PostMapping("/add")
//    public User addNewUser(@RequestParam("name") String name, @RequestParam("password") String password){
//        return userServise.setUsers(name,password);
//   }


}
