package kz.attractor.microgram.DTO;

import kz.attractor.microgram.Model.Comment;
import kz.attractor.microgram.Model.User;
import kz.attractor.microgram.util.Generator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)

public class CommentDTO {
    private UserDTO userId;
    private String comment;
    private LocalDateTime dateTime;

    public static CommentDTO random(Comment comment){
        return builder().comment(Generator.makeComment())
                .dateTime(comment.getDateTime())
                .userId(UserDTO.from(comment.getUserId())).build();
    }

}
