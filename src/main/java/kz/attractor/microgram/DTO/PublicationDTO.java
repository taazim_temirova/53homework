package kz.attractor.microgram.DTO;

import kz.attractor.microgram.Model.Publication;
import kz.attractor.microgram.Model.PublicationImage;
import kz.attractor.microgram.Model.User;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.LocalDateTime;


@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
//@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PublicationDTO {

//    @Id
//    @Builder.Default
//    private  String id = null;
    @Indexed
    private User userId;
    private PublicationImage images;
    @Indexed
    private  String description;
    private LocalDateTime dateTime;
    public static PublicationDTO from(Publication publication) {
        return builder()
//                .id(publication.getId())
                .userId(publication.getUserId())
                .images(publication.getImages())
                .description(publication.getDescription())
                .dateTime(publication.getDateTime())
                .build();
    }
}
