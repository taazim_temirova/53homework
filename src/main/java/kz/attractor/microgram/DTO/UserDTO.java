package kz.attractor.microgram.DTO;


import kz.attractor.microgram.Model.User;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDTO {
    private  String name;
    private  String email;
    private  String password;
    private int publicationCount;
    private int subscriptionsCount;
    private int subscribersCount;

    public  static UserDTO from(User user){
        return builder()
//                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .publicationCount(user.getPublicationCount())
                .subscriptionsCount(user.getSubscriptionsCount())
                .subscribersCount(user.getSubscribersCount())
                .build();
    }

}
