package kz.attractor.microgram.Model;

import kz.attractor.microgram.util.Generator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@Document(collection = "comments")
public class Comment {
    @Id
    @Builder.Default
    private int id = Integer.parseInt(Long.toString(new Date().getTime()).substring(4));
    @DBRef
    private User userId;
    private String comment;
    private LocalDateTime dateTime;

    public Comment(User userId, String comment) {
        this.userId = userId;
        this.comment = comment;
        dateTime = LocalDateTime.now();
    }

    public  static  Comment random(){
        return builder().comment(Generator.makeComment()).build();
    }


    }