package kz.attractor.microgram.Model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
@Document
public class ILikeIt {
    @Id
    @Builder.Default
    private int id = Integer.parseInt(Long.toString(new Date().getTime()).substring(4));
    @DBRef
    private User userId;
    @DBRef
    private Publication publicationId;

    private LocalDateTime likeTime;

    public ILikeIt(User userId, Publication publicationId) {
        this.userId = userId;
        this.publicationId = publicationId;
        likeTime = LocalDateTime.now();
    }


    }
