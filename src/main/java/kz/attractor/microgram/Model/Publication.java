package kz.attractor.microgram.Model;

import kz.attractor.microgram.util.Generator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@Document(collection = "publications")
//@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Builder
public class Publication {

    @Id
    @Builder.Default
    private int id = Integer.parseInt(Long.toString(new Date().getTime()).substring(4));
    @Indexed
    @DBRef
    private User userId;
    @DBRef
    private  PublicationImage images;
    private  String description;
    private LocalDateTime dateTime;

    @DBRef
    private Comment comments;
    @DBRef
    private ILikeIt likes;

//    public  Publication(User userId , Comment comment, ILikeIt likes){
//        this.userId =userId;
//        this.comments = comment;
//        this.likes=likes;
//        this.description = Generator.makeDescription();
//        this.dateTime =LocalDateTime.now();
//
//    }

    public  static  Publication random(){
        return builder().description(Generator.makeDescription())
                .build();
    }


}
