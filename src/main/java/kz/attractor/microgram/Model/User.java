package kz.attractor.microgram.Model;

import lombok.*;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import kz.attractor.microgram.util.Generator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Data
@Builder
@AllArgsConstructor
@Document(collection="users")
public class User implements UserDetails {
    @Id
    @Builder.Default
    private int id = Integer.parseInt(Long.toString(new Date().getTime()).substring(4));
    private String name;
    private String email;
    private String password;
    private int publicationCount = 0;
    private int subscriptionsCount = 0;
    private int subscribersCount = 0;

//    public User( String username, String password, int publicationCount , int subscribersCount, int subscriptionsCount ) {
//        this.name = username;
//        this.password = password;
//        this.publicationCount = publicationCount;
//        this.subscribersCount =subscribersCount;
//        this.subscriptionsCount=subscriptionsCount;
//    }
//    public  User(String name, String password){
//        this.name = name;
//        this.password = password;
//    }

    public  static  User random(){
        return builder().email(Generator.makeEmail())
                .name(Generator.makeName())
                .password(Generator.makePassword())
                .build();
    }

    @Override
    public Collection<? extends GrantedAuthority>
    getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }

    @Override
    public String getUsername() {
        return name;
    }
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}


