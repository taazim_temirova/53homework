package kz.attractor.microgram.Model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Document
public class UserSubscriptions {

    @Id
    private int id = Integer.parseInt(Long.toString(new Date().getTime()).substring(4));
    @DBRef
    private  User userIdr1;
    @DBRef
    private User userId2;

    private LocalDateTime eventsDate;

    }
