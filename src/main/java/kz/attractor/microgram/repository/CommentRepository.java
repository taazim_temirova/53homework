package kz.attractor.microgram.repository;

import kz.attractor.microgram.Model.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, String> {
}
