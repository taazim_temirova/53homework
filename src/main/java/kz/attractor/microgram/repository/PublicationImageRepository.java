package kz.attractor.microgram.repository;

import kz.attractor.microgram.Model.PublicationImage;
import org.springframework.data.repository.CrudRepository;

public interface PublicationImageRepository  extends CrudRepository<PublicationImage, String> {
}
