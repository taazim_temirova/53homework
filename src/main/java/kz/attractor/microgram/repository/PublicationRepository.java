package kz.attractor.microgram.repository;

import kz.attractor.microgram.Model.Publication;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PublicationRepository extends CrudRepository<Publication, String> {

Slice<Publication>findByUserId(int userId, Pageable pageable);

    Slice<Publication> findByUserId(String userId, Pageable pageable);
}
