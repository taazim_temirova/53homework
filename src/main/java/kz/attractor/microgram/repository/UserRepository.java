package kz.attractor.microgram.repository;

import kz.attractor.microgram.Model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User,String> {

    public Iterable<User> findAllById(Sort s);

    public Iterable<User> findAllById(int year, Sort s);

    @Query("{'name' : { '$gte' : ?0, '$lte' : ?1 }}")
    public Iterable<User> findAllByName(String name, Sort s);

//    public  Iterable<User> findAll(Pageable pageable);
    public Optional<User> findByEmail(String email);
    public Optional<User> findByName(String s);

}