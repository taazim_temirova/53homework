package kz.attractor.microgram.service;

import kz.attractor.microgram.Model.PublicationImage;
import  kz.attractor.microgram.DTO.*;
import kz.attractor.microgram.ResourceNotFoundException.ResourceNotFoundException;
import kz.attractor.microgram.repository.PublicationImageRepository;
import kz.attractor.microgram.repository.PublicationRepository;
import org.bson.types.Binary;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class PubImageService {
    private final PublicationImageRepository movieImageRepository;

    public PubImageService(PublicationImageRepository movieImageRepository) {
        this.movieImageRepository = movieImageRepository;
    }

    public PubImageDTO addImage(MultipartFile file) {
        byte[] data = new byte[0];
        try {
            data = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (data.length == 0) {
            // TODO return no content or something or throw exception
            //  which will be processed on controller layer
        }

        Binary bData = new Binary(data);
        PublicationImage image = PublicationImage.builder().posterData(bData).build();

        movieImageRepository.save(image);

        return PubImageDTO.builder()
                .imageId(image.getId())
                .build();
    }

    public Resource getById(String imageId) {
        PublicationImage publicationImage = movieImageRepository.findById(imageId)
                .orElseThrow(() -> new ResourceNotFoundException("Movie Image with " + imageId + " doesn't exists!"));
        return new ByteArrayResource(publicationImage.getPosterData().getData());
    }
}
