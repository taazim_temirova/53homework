package kz.attractor.microgram.service;

import kz.attractor.microgram.DTO.PublicationDTO;
import kz.attractor.microgram.Model.Publication;
import kz.attractor.microgram.repository.PublicationRepository;
import kz.attractor.microgram.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class PublicationService {
    private final PublicationRepository publicationRepository;
    private final UserRepository userRepository;

    public PublicationService(PublicationRepository publicationRepository, UserRepository userRepository) {
        this.publicationRepository = publicationRepository;
        this.userRepository = userRepository;
    }

    public PublicationDTO addPublication(/* From Spring Security! User user, */PublicationDTO publicationDTO) {
        //TODO one user can have only one review per movie. add check
        var user = userRepository.findByEmail("demo@demo").get();
        var review = Publication.builder()
//                .id(publicationDTO.getId())
                .userId(publicationDTO.getUserId())
                .images(publicationDTO.getImages())
                .description(publicationDTO.getDescription())
                .dateTime(publicationDTO.getDateTime())
                .build();
        publicationRepository.save(review);

        // TODO recalculate rating after save. Update movie document

        return PublicationDTO.from(review);
    }

    public Slice<PublicationDTO> findReviews(String userId, Pageable pageable) {
        var slice = publicationRepository.findByUserId(userId, pageable);
        return slice.map(PublicationDTO::from);
    }

    public boolean deleteReview(String reviewId) {
        //TODO recalculate movie rating before delete
        publicationRepository.deleteById(reviewId);
        return true;
    }
}
