package kz.attractor.microgram.service;

import kz.attractor.microgram.DTO.UserDTO;
import kz.attractor.microgram.Model.User;
import kz.attractor.microgram.ResourceNotFoundException.ResourceNotFoundException;
import kz.attractor.microgram.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {
    @Autowired
    private final UserRepository userRepository;
//    private final UserDTO userDTO;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByName(s);
        if (user.isPresent())
            return   user.get();

        throw new UsernameNotFoundException("User does not exit");
    }

//    public UserDTO addUsers( UserDTO userDTO,String name, String password){
//
//        var user = User.builder()
////                .id(userDTO.getId())
//                .name(name)
//                .password(password)
//                .email(userDTO.getEmail())
//                .publicationCount(userDTO.getPublicationCount())
//                .subscriptionsCount(userDTO.getSubscriptionsCount())
//                .subscribersCount(userDTO.getSubscribersCount())
//                .build();
//        userRepository.save(user);
//        return UserDTO.from(user);
//    }

//    public User setUsers(String name, String password) {
//        return userRepository.save(new User( name, password));
//    }



    public Slice<UserDTO> findUsers(Pageable p) {
        var slice = userRepository.findAll(p);
        return slice.map(UserDTO::from);
    }

    public UserDTO findOne(String movieId) {
        var movie = userRepository.findById(movieId)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find movie with the ID: " + movieId));
        return UserDTO.from(movie);
    }



//    public UserDTO addUser(UserDTO userDTO) {
//        var user = User.builder()
////                .id(userDTO.getId())
//                .name(userDTO.getName())
//                .email(userDTO.getEmail())
//                .password(userDTO.getPassword())
//                .publicationCount(userDTO.getPublicationCount())
//                .subscriptionsCount(userDTO.getSubscriptionsCount())
//                .subscribersCount(userDTO.getSubscribersCount())
//                .build();
//        userRepository.save(user);
//        return UserDTO.from(user);
//
//    }
}

