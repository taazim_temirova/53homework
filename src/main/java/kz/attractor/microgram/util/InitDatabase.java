package kz.attractor.microgram.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import kz.attractor.microgram.Model.Comment;
import kz.attractor.microgram.Model.Publication;
import kz.attractor.microgram.Model.User;
import kz.attractor.microgram.repository.CommentRepository;
import kz.attractor.microgram.repository.PublicationRepository;
import kz.attractor.microgram.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

@Configuration
public class InitDatabase {

    private static final Random r = new Random();

    @Bean
    CommandLineRunner init(UserRepository userRepo, PublicationRepository publicationRepo, CommentRepository commentRepo) {
        return (args) -> {
            userRepo.deleteAll();
            publicationRepo.deleteAll();
            commentRepo.deleteAll();

            List<User> users = Stream.generate(User::random)
                    .limit(5)
                    .peek(System.out::println).collect(toList());
            userRepo.saveAll(users);
//
            List<Publication> publications = Stream.generate(Publication::random)
                    .limit(5)
                    .peek(System.out::println)

                    .collect(toList());
            publicationRepo.saveAll(publications);

            List<Comment> comments = Stream.generate(Comment::random).limit(5)
                    .peek(System.out::println)
                    .collect(toList());
            commentRepo.saveAll(comments);

            List<Publication> publications1 = new ArrayList<>();

            publications.forEach(user -> {
                selectRandomUsers(users, r.nextInt(3)+1).stream()
                        .map(user1 -> Publication.random())
                        .peek(publications1::add)
                        .forEach(publicationRepo::save);
            });
        };
    }


//    private User[] candidates() {
//        return new User[]{
//                new User("admin", new BCryptPasswordEncoder().encode("test123"), 4,5,6),
//                new User( "user1", new BCryptPasswordEncoder().encode("123user"),5,4,3),
//                new User( "guest", new BCryptPasswordEncoder().encode("guest2"),3,4,5)};
//    }

    private List<User> selectRandomUsers(List<User> users, int amountOfMovies) {
        return Stream.generate(() -> pickRandom(users))
                .distinct()
                .limit(amountOfMovies)
                .collect(toList());
    }

    private static User pickRandom(List<User> users) {
        return users.get(r.nextInt(users.size()));
    }

    private static List<User> readMovies(String fileName) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            var data = Files.readString(Paths.get(fileName));
            var listType = new TypeReference<List<User>>(){};
            return mapper.readValue(data, listType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return List.of();
    }
}
