//package kz.attractor.microgram.util;
//
//import kz.attractor.microgram.Model.User;
//import kz.attractor.microgram.repository.UserRepository;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//
//import java.util.stream.Stream;
//
//@Configuration
//public class PreloadDataBase {
//
//    @Bean
//    CommandLineRunner initDatabase(UserRepository repository) {
//        repository.deleteAll();
//        return (args) -> Stream.of(users())
//                .forEach(repository::save);
//    }
//    private User[] users() {
//        return new User[]{
//                new User(1, "Bakulya", "bakulya@gmail.com", "baktygul", 4,5,6),
//                new User(2, "Ainura", "ainura@gmail.com", "ainura", 5, 6,4),
//                new User(3, "Aika", "aika@gamail.com", "aika", 4 , 3,4)};
//    }
//
//
//}
